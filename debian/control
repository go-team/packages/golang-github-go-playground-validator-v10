Source: golang-github-go-playground-validator-v10
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Cyril Brulebois <cyril@debamax.com>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-github-go-playground-assert-v2-dev,
               golang-github-go-playground-locales-dev (>= 0.14.0~),
               golang-github-go-playground-universal-translator-dev,
               golang-github-leodido-go-urn-dev,
               golang-golang-x-crypto-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-go-playground-validator-v10
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-go-playground-validator-v10.git
Homepage: https://github.com/go-playground/validator
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/go-playground/validator/v10

Package: golang-github-go-playground-validator-v10-dev
Architecture: all
Depends: golang-github-go-playground-locales-dev (>= 0.14.0~),
         golang-github-go-playground-universal-translator-dev,
         golang-github-leodido-go-urn-dev,
         golang-golang-x-crypto-dev,
         ${misc:Depends}
Description: Go library for struct and field validation
 The validator package implements value validations for structs and
 individual fields based on tags.
 .
 It has the following unique features:
  - Cross Field and Cross Struct validation by using validation tags
    or custom validators.
  - Slice, Array and Map diving, which allows any or all levels of a
    multidimensional field to be validated.
  - Ability to dive into both map keys and values for validation.
  - Handles type interface by determining its underlying type prior
    to validation.
  - Handles custom field types such as the SQL driver's Valuer.
  - Alias validation tags, which allows for mapping of several
    validations to a single tag for easier definition of validations
    on structs.
  - Extraction of custom defined Field Name, allowing to specify
    extracting the JSON name while validating and making it available
    in the resulting FieldError.
  - Customizable i18n-aware error messages.
